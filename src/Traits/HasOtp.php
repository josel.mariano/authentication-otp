<?php

namespace Outgive\AuthenticationOtp\Traits;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

trait HasOtp
{
    public function otp()
    {
        return $this->hasOne(
            '\Outgive\AuthenticationOtp\Models\Auth\Otp',
            'user_id',
            'id'
        );
    }
}