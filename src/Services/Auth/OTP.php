<?php

namespace Outgive\AuthenticationOtp\Services\Auth;

/**
 * Vendors
 */
use Illuminate\Http\Request;
use Carbon\Carbon;

/**
 * Stallion APP
  */
use Auth;
use App\Models\User;

/**
 * Package
 */
use Outgive\AuthenticationOtp\Http\OTPResponse;
use Outgive\AuthenticationOtp\Exceptions\OTPException;
use outgive\AuthenticationLog\AuthenticationLog;
use Outgive\AuthenticationOtp\Models\Auth\Otp as AuthOtp;
use Outgive\AuthenticationOtp\Notifications\SendOtp;

/**
 * This class describes an auth otp.
 */
class OTP
{
    /**
     * @var \App\Models\User $user
     */
    protected  $user = false;

    /**
     * @var int $otp_code_length
     */
    public  $otp_code_length;

    /**
     * @var array $force_positions
     */
    public  $force_positions;

    /**
     * @var int $expire_seconds
     */
    public  $expire_seconds;

    /**
     * @var array $response
     */
    public  $response = array();

    private $status;

    const STATUS_READY           = 'ready';
    const STATUS_VERIFIED        = 'verified';
    const STATUS_VALID_OTP       = 'valid_otp';
    const STATUS_INVALID_OTP     = 'invalid_otp';
    const STATUS_EXPIRED_OTP     = 'expired_otp';
    const STATUS_ALREADY_VERFIED = 'already_verified';
    const OTP_DISABLED           = 'otp_disabled';

    const VIA_EMAIL = '0';
    const VIA_SMS   = '1';

    /**
     * Constructs a new instance.
     */
    public function __construct()
    {
        $this->otp_code_length   = config('otp.code_length');
        $this->force_positions   = config('otp.force_positions');
        $this->expire_seconds    = config('otp.expire_seconds');
        $this->force_expire_days = config('otp.force_expire_days');
        $this->enabled           = config('otp.enabled');
        $this->response          = (new OTPResponse())->getReadyResponse();
    }

    /**
     * Determines if enabled.
     *
     * @return     bool  True if enabled, False otherwise.
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function isStatusReady()
    {
        return (boolean) $this->getStatus() == self::STATUS_READY;
    }

    /**
     * Determines if otp still good.
     *
     * @param      User  $user   The user
     *
     * @return     bool    True if the otp is verified and not obsolete, False otherwise.
     */
    public function isOtpStillGood(User $user, Request $request)
    {
        $auth_otp = $this->getUserOtp($user->id);

        if (!$auth_otp->is_verified && $auth_otp->isExpiredInSeconds()) {
            return false;
        }

        /**
         * If somehow user is verified but got old otp
         */
        if ($auth_otp->isObsolete()) {
            return false;
        }

        /**
         * Is a Stranger
         */
        if ($this->isStranger($user, $request)) {
            return false;
        }

        return true;
    }

    /**
     * Determines if user needs otp.
     *
     * @param      \Illuminate\Http\Request  $request  The request
     *
     * @return     boolean                   True if user needs otp, False otherwise.
     */
    public function isUserNeedsOTP(Request $request)
    {
        /**
         * No one needs otp if it's disabled
         */
        if (!$this->isEnabled()) {
            return false;
        }

        /**
         * Dont proceed if you don't have valid user
         */
        if (!$this->isUserValid()) {
            return false;
        }

        $user     = $this->getUser();
        $auth_otp = $this->getUserOtp($user->id);

        if (!$auth_otp) {
            return false;
        }

        /* If no otp found then it doesn't need otp */
        if (!$auth_otp->otp_enabled) {
            return false;
        }

        if ($auth_otp->is_verified && $this->isOtpStillGood($user, $request)) {
            return false;
        }

        return true;
    }

    /**
     * Gets the auth log.
     *
     * @param      \App\Models\User                             $user        The user
     * @param      string                                       $ip_address  The ip address
     * @param      string                                       $user_agent  The user agent
     *
     * @return     outgive\AuthenticationLog\AuthenticationLog  The auth log.
     */
    public function getAuthLog(User $user, string $ip_address, string $user_agent)
    {
        return $user->authentications()
            ->whereIpAddress($ip_address)
            ->whereUserAgent($user_agent)
            ->first();
    }

    /**
     * Determines if user valid.
     *
     * @return     boolean  True if user valid, False otherwise.
     */
    public function isUserValid()
    {
        return (boolean)$this->user;
    }

    /**
     * Sets the user.
     *
     * @param      \App\Models\User  $user   The user
     *
     * @return     self              self instance
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Resolve user by credentials and fetch the user by email
     *
     * @param      array  $credentials  The credentials
     *
     * @return     self    self instance
     */
    public function resolveUser(array $credentials)
    {
        if (!Auth::validate($credentials)) {
            return $this;
        }
        $this->user = User::where('email', $credentials['email'])->first();
        return $this;
    }

    /**
     * Determines if stranger.
     */
    public function isStranger(User $user, Request $request)
    {
        $known = $this->getAuthLog($user, $request->ip(), $request->userAgent());
        return ($user->authentications()->count() > 0 && !$known);
    }

    /**
     * Determines whether the specified user is disabled for user.
     *
     * @param      \App\Models\User  $user   The user
     *
     * @return     bool              True if the specified user is disabled for user, False otherwise.
     */
    public function isDisabledForUser(User $user)
    {
        $auth_otp = $this->getUserOtp($user->id);

        if (is_null($auth_otp)) {
            return true;
        }

        return !$auth_otp->otp_enabled;
    }

    /**
     * Process the user's OTP
     *
     * @param      \Illuminate\Http\Request                            $request    The request
     * @param      bool                                                $is_verify  Indicates if verify
     *
     * @throws     \Outgive\AuthenticationOtp\Exceptions\OTPException  (description)
     *
     * @return     string                                              OTP STATU
     */
    public function processOtp(Request $request, $is_verify = true)
    {
        $otp_code = $request->post('code');

        /**
         * Create OTP if not exists and notify user
         */
        $user     = $this->getUser();
        $auth_otp = $this->getUserOtp($user->id);

        if ($this->isDisabledForUser($user) && $is_verify) {
            $this->setStatus(self::OTP_DISABLED);
            throw new OTPException(
                'OTP feature is disabled for this user',
                self::OTP_DISABLED
            );
        }

       // if (!$this->isOtpStillGood($user, $request)) {
       //      $this->setStatus(self::STATUS_EXPIRED_OTP);
       //      throw new OTPException(
       //          'OTP was expired, but resent',
       //          self::STATUS_EXPIRED_OTP
       //      );
       // }

        $verify = $this->verify($otp_code);

        /**
         * Generates the authentication log to avoid login failure due to
         * unknown device by this otp feature
         */
        $this->createAuthenticationLog(
            $user, $request->ip(), $request->userAgent()
        );

        return $verify;
    }


    /**
     * Verify the OTP code if it's correct per user
     *
     * @param      string      $code   The code
     *
     * @return     boolean  True if verified, false otherwise
     */
    public function verify(string $code)
    {
        $user     = $this->getUser();
        $auth_otp = $this->getUserOtp($user->id);

        if (!$auth_otp) {
            $this->setStatus(self::STATUS_INVALID_OTP);
            throw new OTPException('Invalid OTP', self::STATUS_INVALID_OTP);
        }

        if ($auth_otp->code != $code) {
            $this->setStatus(self::STATUS_INVALID_OTP);
            throw new OTPException('Invalid OTP', self::STATUS_INVALID_OTP);
        }

        /**
         * Measure if expired in seconds
         */
        if ($auth_otp->isExpiredInSeconds()) {
            $this->setStatus(self::STATUS_EXPIRED_OTP);
            throw new OTPException('OTP expired', self::STATUS_EXPIRED_OTP);
        }

        /**
         * We're good here, now so we flag it as verified
         */
        $auth_otp = $this->saveUserOtp($user, [
            'is_verified' => 1,
            'code'        => $code,
        ]);

        return $auth_otp;
    }

    /**
     * Creates an authentication log.
     *
     * @param      \App\Models\User  $user        The user
     * @param      string            $ip_address  The ip address
     * @param      string            $user_agent  The user agent
     */
    public function createAuthenticationLog($user, $ip_address, $user_agent)
    {
        $authenticationLog = new AuthenticationLog([
            'ip_address' => $ip_address,
            'user_agent' => $user_agent,
            'login_at'   => Carbon::now(),
            'location'   => null,
        ]);

        $user->authentications()->save($authenticationLog);
    }

    /**
     * Gets the user.
     *
     * @return \App\Models\User The user.
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Gets the response.
     *
     * @return     Response  The response.
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Gets the user otp.
     *
     * @param      int                  $user_id  The user identifier
     *
     * @return     App\Models\Auth\Otp  The user otp.
     */
    public function getUserOtp(int $user_id)
    {
        return AuthOtp::where('user_id', $user_id)->first();
    }

    /**
     * Generates the otp code
     *
     * @return     string  The otp code
     */
    public function generateOtpCode()
    {
        $digits = $this->otp_code_length;
        $otp_code = str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);

        /**
         * make sure we get the exact length
         */
        if (strlen((string)$otp_code) != $digits) {
            return $this->generateOtpCode();
        }

        return $otp_code;
    }

    /**
     * Removes user otp codes.
     *
     * @return     self  Self instance
     */
    public function removeUserOtpCodes()
    {
        AuthOtp::where('user_id', $this->user_->id)->delete();
        return $this;
    }

    /**
     * Saves an user otp. This also refresh the existing otp code
     *
     * @param      \App\Models\User     $user     The user
     * @param      array                $options  The options
     *
     * @return     App\Models\Auth\Otp  The otp object
     */
    public function saveUserOtp(User $user, $options = array())
    {
        $search      = array('user_id' => $user->id);
        $update_data = array(
            'email' => $user->email,
            'phone' => $user->phone,
        );

        $update_data = array_merge($update_data, $options);
        $auth_otp    = AuthOtp::updateOrCreate($search, $update_data);
        return $auth_otp;
    }

    /**
     * Sends an otp.
     *
     * @return     self
     */
    public function sendOtp()
    {
        $user     = $this->getUser();
        $auth_otp = $this->saveUserOtp($user, [
            'code'        => $this->generateOtpCode(),
            'is_verified' => 0,
        ]);

        $user->notify(new SendOtp($auth_otp));

        $this->setStatus(self::STATUS_READY);
        return $this;
    }

    /**
     * Sends otp sms.
     *
     * @deprecated Will be deprecate next release, cause of self::sendOtp
     */
    public function sendOtpSms()
    {
        $auth_otp = $this->saveUserOtp($this->user, [
            'code'        => $this->generateOtpCode(),
            'is_verified' => 0,
        ]);


        $auth_otp->otp_via = self::VIA_SMS;
        $this->user->notify(new SendOtp($auth_otp));

        /**
         * @deprecated v2 Converted manual nexmo to notifiable
         *
         * Avoid incurring charges to NEXMO API when unit-testing
         */
        // if (!\App::runningUnitTests()) {
        //     $message = $this->client->message()->send([
        //         'to'   => $this->user->phone,
        //         'from' => $this->registered_number,
        //         'text' => $this->getMessage($auth_otp->code)
        //     ]);
        // }

        $this->setStatus(self::STATUS_READY);
        return $this;
    }

    /**
     * Sends otp to email.
     *
     * @deprecated Will be deprecate next release, cause of self::sendOtp
     */
    public function sendOtpEmail()
    {
        $auth_otp = $this->saveUserOtp($this->user, [
            'code'        => $this->generateOtpCode(),
            'is_verified' => 0,
        ]);

        $auth_otp->otp_via = self::VIA_EMAIL;
        $this->user->notify(new SendOtp($auth_otp));

        $this->setStatus(self::STATUS_READY);
        return $this;
    }
}