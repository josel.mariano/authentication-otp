<?php

namespace Outgive\AuthenticationOtp\Models\Auth;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Otp extends Model
{
    protected $table = 'user_otp';
    protected $primaryKey = 'id';

    const VIA_EMAIL = '0';
    const VIA_SMS   = '1';

    const NOTIFY_EMAIL = 'mail';
    const NOTIFY_SMS   = 'nexmo';

    protected $hidden = [
        'code'
    ];

    protected $fillable = [
        'id',
        'user_id',
        'email',
        'phone',
        'code',
        'is_verified',
        'updated_at',
        'otp_enabled',
        'otp_via',
    ];

    public function user()
    {
        return $this->hasOne('\App\Models\User', 'id', 'user_id');
    }

    /**
     * Gets the notifiable via.
     *
     * @return     string  The notifiable via.
     */
    public function getNotifiableVia()
    {
        return ($this->otp_via == self::VIA_SMS) ?
            self::NOTIFY_SMS: self::NOTIFY_EMAIL;
    }

    /**
     * Determines if expired in seconds.
     *
     * @return     bool  True if expired in seconds, False otherwise.
     */
    public function isExpiredInSeconds()
    {
        $start_time  = Carbon::parse($this->updated_at);
        $finish_time = Carbon::now();
        $total_duration = $finish_time->diffInSeconds($start_time);
        return config('otp.expire_seconds') < $total_duration;
    }

    /**
     * Determines if obsolete, means that even if it's verified it will check on
     * the forced expire in days
     *
     * @return     bool  True if obsolete, False otherwise.
     */
    public function isObsolete()
    {
        $start_time     = Carbon::parse($this->updated_at);
        $finish_time    = Carbon::now();
        $total_duration = $finish_time->diffInDays($start_time);
        $is_expired     = ((int)$total_duration > config('otp.force_expire_days'));
        $is_obsolete    = ($this->is_verified && $is_expired);

        return $is_obsolete;
    }
}
