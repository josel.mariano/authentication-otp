<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class V2UpgradeUserOtpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_otp', function (Blueprint $table) {
            $table->boolean('otp_enabled')->default(0);
            $table->string('otp_via', 16)->default('nexmo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_otp', function (Blueprint $table) {
            $table->dropColumn('otp_enabled');
            $table->dropColumn('otp_via');
        });
    }
}
