<?php

namespace Outgive\AuthenticationOtp\Exceptions;

use Exception;

/**
 * Exception for signaling otp errors.
 */
class OTPException extends Exception
{
    public $status;
    public $context;

    public function __construct (
        $message,
        $status,
        $context = [],
        $code = 0,
        Exception $previous = null
    ) {
        $this->status = $status;
        $this->context = $context;
        parent::__construct($message, $code, $previous);
    }

    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getContext()
    {
        return $this->context;
    }
}