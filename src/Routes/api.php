<?php
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'auth'], function () {
    Route::get('otp/length', 'Auth\OtpController@length');
    Route::post('otp/check', 'Auth\OtpController@check');
    Route::get('otp/refresh', 'Auth\OtpController@refresh')
            ->middleware('throttle:5,1');
    Route::get('otp/email', 'Auth\OtpController@email')
            ->middleware('throttle:5,1');
    Route::post('otp/verify', 'Auth\OtpController@verify')
            ->middleware('throttle:5,1');
});
Route::group(['middleware' => ['web','jwt.auth']], function () {
    Route::group(['prefix' => 'auth'], function () {
        Route::post('otp/enable', 'Auth\OtpController@enable');
        Route::post('otp/disable', 'Auth\OtpController@disable');
        Route::post('otp/activate', 'Auth\OtpController@activate');
    });
});
