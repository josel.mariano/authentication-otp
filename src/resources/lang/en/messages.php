<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication OTP Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication otp for
    | various messages that we need to display to the user. You are free to
    | modify these language lines according to your application's requirements.
    |
    */

    'subject' => 'Login OTP Code',
    'content' => 'Your :app account log-in otp code.',
    'sms' => 'Hello from :app, your OTP CODE is :code'

];
