@component('mail::message')
# Hello!

Your {{ config('app.name') }} account log-in otp code.

> **Account:** {{ $account->email }}<br>
> **Time:** {{ $time->toCookieString() }}<br>
> **OTP CODE:** {{ $code }}

This OTP code may expire in {{ $expire }} minutes

Regards,<br>{{ config('app.name') }}
@endcomponent
