<?php

namespace Outgive\AuthenticationOtp\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\NexmoMessage;
use Outgive\AuthenticationOtp\Models\Auth\Otp as AuthOtp;

class SendOtp extends Notification
{
    use Queueable;

    /**
     * The authentication otp.
     *
     * @var \Outgive\AuthenticationOtp\Models\Auth\Otp
     */
    public $auth_otp;

    /**
     * Create a new notification instance.
     *
     * @param  \Outgive\AuthenticationOtp\Models\Auth\Otp  $auth_otp
     * @return void
     */
    public function __construct(AuthOtp $auth_otp)
    {
        $this->auth_otp = $auth_otp;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [$this->auth_otp->getNotifiableVia()];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $fields = [
            'account' => $notifiable,
            'time'    => $this->auth_otp->updated_at,
            'code'    => $this->auth_otp->code,
            'expire'  => floor((config('otp.expire_seconds') / 60) % 60),
        ];

        return (new MailMessage)
            ->subject(trans('authentication-otp::messages.subject'))
            ->markdown('authentication-otp::emails.otp', $fields);
    }

    /**
     * Get the Nexmo / SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\NexmoMessage
     */
    public function toNexmo($notifiable)
    {
        $content = trans('authentication-otp::messages.sms', [
            'app'  => config('app.name'),
            'code' => $this->auth_otp->code,
        ]);

        return (new NexmoMessage)
            ->content(
                trans('authentication-otp::messages.sms',
                    [
                        'app'  => config('app.name'),
                        'code' => $this->auth_otp->code,
                    ]
                )
            );
    }
}
