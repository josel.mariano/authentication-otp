<?php

namespace Outgive\AuthenticationOtp\Http\Controllers\Auth;

/**
 * Vedors
 */
use Illuminate\Http\Request;
use Throwable;

/**
 * Apps
 */
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
// use Auth;

/**
 * Package
 */
use Outgive\AuthenticationOtp\Services\Auth\OTP as OTPService;
use Outgive\AuthenticationOtp\Http\OTPResponse;

/**
 * Controls the data flow into an otp object and updates the view whenever data changes.
 */
class OtpController extends Controller
{
    private $otp_service;
    private $otp_response;

    public function __construct (
        OTPService $otp_service,
        OTPResponse $otp_response
    )
    {
        $this->otp_service = $otp_service;
        $this->otp_response = $otp_response;
    }

    private function getOtpService()
    {
        return $this->otp_service;
    }

    private function getOtpResponse()
    {
        return $this->otp_response;
    }

    /**
     * Just a way to check the status to separate the throttling
     */
    public function check(Request $request)
    {
        $validatedData = $request->validate([
            'email'    => 'required|email|max:255',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');
        $otp_service = $this->getOtpService();

        /**
         * let api know it is disabled
         */
        if (!$otp_service->isEnabled()) {
            return $this->getOtpResponse()->getDisabledResponse();
        }

        /**
         * Resolve user
         */
        $otp_service->resolveUser($credentials);

        if (!$otp_service->isUserValid()) {
            return $this->getOtpResponse()->getUserNotFound();
        }

        $user = $otp_service->getUser();

        /**
         * Check if disabled for user
         */
        if ($otp_service->isDisabledForUser($user)) {
            return $this->getOtpResponse()->getDisabledForUser();
        }

        try {
            /**
             * Chec if still good or send an OTP
             */
            $otp_service->isOtpStillGood($user, $request);
            $auth_otp = $otp_service->getUserOtp($user->id);
            if (
                $otp_service->isOtpStillGood($user, $request) &&
                $auth_otp->is_verified
            ) {
                return $this->getOtpResponse()->getAlreadyVerified();
            }

            if ($auth_otp->isExpiredInSeconds()) {
                $otp_service->sendOtp();
            }

            if (
                $otp_service->isStranger($user, $request) &&
                $auth_otp->isExpiredInSeconds()
            ) {
                $otp_service->sendOtp();
            }

            return $this->getOtpResponse()->getReadyResponse();
        } catch (Throwable $error) {
            return $this
                ->getOtpResponse()
                ->getErrorResponse($error);
        }

    }

    /**
     * Verify an users otp code also behaves like login, but does not geenrate
     * token
     *
     * @param      \Illuminate\Http\Request  $request      The request
     * @param      OTPService                $otp_service  The otp service
     *
     * @return     void
     */
    public function verify(Request $request)
    {
        $validatedData = $request->validate([
            'email'    => 'required|email|max:255',
            'password' => 'required',
            'code'     => 'required',
        ]);

        $credentials = $request->only('email', 'password');
        $otp_service = $this->getOtpService();

        /**
         * let api know it is disabled
         */
        if (!$otp_service->isEnabled()) {
            return $this->getOtpResponse()->getDisabledResponse();
        }

        /**
         * Resolve user
         */
        $otp_service->resolveUser($credentials);

        if (!$otp_service->isUserValid()) {
            return $this->getOtpResponse()->getUserNotFound();
        }

        // /**
        //  * Check if disabled for user
        //  */
        // if (!$otp_service->getUser()->otp_enabled) {
        //     return $this->getOtpResponse()->getDisabledForUser();
        // }

        try {
            $otp_service->processOtp($request);
            return $this
                ->getOtpResponse()
                ->getVerifiedResponse();
        } catch (Throwable $error) {
            return $this
                ->getOtpResponse()
                ->getErrorResponse($error);
        }
    }

    /**
     * Refresh an otp by user
     *
     * @param Request $request
     * @return void
     */
    public function refresh(Request $request)
    {
        $user = User::where('email', $request->query('email'))->first();

        if (!$user) {
            return $this->getOtpResponse()->getUserNotFound();
        }

        try {
            $otp_service = $this->getOtpService();
            $otp_service->setUser($user);
            $otp_service->sendOtpSms();

            return $this->getOtpResponse()->getReadyResponse();
        } catch (Throwable $error) {
            return $this
                ->getOtpResponse()
                ->getErrorResponse($error);
        }

    }

    /**
     * Refresh an otp by user, but send it to users email
     *
     * @param Request $request
     * @return void
     */
    public function email(Request $request)
    {
        $user = User::where('email', $request->query('email'))->first();

        if (!$user) {
            return $this->getOtpResponse()->getUserNotFound();
        }

        try {
            $otp_service = $this->getOtpService();
            $otp_service->setUser($user);
            $otp_service->sendOtpEmail();

            return $this->getOtpResponse()->getReadyResponse();
        } catch (Throwable $error) {
            return $this
                ->getOtpResponse()
                ->getErrorResponse($error);
        }
    }

    /**
     * Get the otp length
     *
     * @param Request $request
     * @return void
     */
    public function length()
    {
        return response([
            'otp_length' => config('otp.code_length'),
            'status'     => 'success',
            'success'    => true,
        ]);
    }

    /**
     * Enable user OTP
     *
     * @param Request $request
     * @return void
     */
    public function enable(Request $request)
    {
        $user    = Auth::user();
        $otp_via = $request->post('otp_via');

        if (!$user) {
            return $this->getOtpResponse()->getUserNotFound();
        }

        if ($otp_via == OTPService::VIA_SMS) {
            $validatedData = $request->validate([
                'phone' => 'required',
            ]);
            $user->phone = $request->post('phone');
        }


        try {
            $user->otp_enabled = 0;
            $user->save();

            $otp_service = $this->getOtpService();
            $auth_otp    = $otp_service->saveUserOtp($user, [
                'otp_enabled' => 0,
                'otp_via'     => $otp_via,
                'phone'       => $user->phone
            ]);

            $otp_service
                ->setUser($user)
                ->sendOtp();

            return $this->getOtpResponse()->getReadyResponse();
        } catch (Throwable $error) {
            return $this
                ->getOtpResponse()
                ->getErrorResponse($error);
        }

    }

    /**
     * Disable user OTP
     *
     * @param Request $request
     * @return void
     */
    public function disable(Request $request)
    {

        $user = Auth::user();

        if (!$user) {
            return $this->getOtpResponse()->getUserNotFound();
        }

        try {
            $user->otp_enabled = 0;
            $user->save();

            $otp_service = $this->getOtpService();
            $otp_service->saveUserOtp($user, [
                'is_verified' => 0,
                'otp_enabled' => 0,
            ]);

            return $this->getOtpResponse()->_getResponse(
                'OTP Code succesfully disabled'
            );
        } catch (Throwable $error) {
            return $this
                ->getOtpResponse()
                ->getErrorResponse($error);
        }

    }

    /**
     * Activates the otp request.
     *
     * @param      \Illuminate\Http\Request  $request  The request
     */
    public function activate(Request $request)
    {
        $validatedData = $request->validate([
            'code' => 'required',
        ]);

        $user        = Auth::user();
        $otp_service = $this->getOtpService();

        if (!$user) {
            return response([
                'otp_status' => 'error',
                'status'     => 'error',
                'msg'        => 'User not found!',
                'success'    => false,
            ]);
        }

        /**
         * let api know it is disabled
         */
        if (!$otp_service->isEnabled()) {
            return $this
                ->getOtpResponse()
                ->getDisabledResponse();
        }

        try {
            $otp_service->setUser($user);
            $otp = $otp_service->processOtp($request, false);

            $otp->otp_enabled = 1;
            $otp->save();

            $user->otp_enabled = 1;
            $user->save();

            return $this
                ->getOtpResponse()
                ->_getResponse('OTP Activated');
        } catch (Throwable $error) {

            $user->otp_enabled = 0;
            $user->save();

            return $this
                ->getOtpResponse()
                ->getErrorResponse($error);
        }
    }

}
