<?php

namespace Outgive\AuthenticationOtp\Http;

/**
 * Vedors
 */
use Illuminate\Http\Request;
use Throwable;

/**
 * Package
 */
use Outgive\AuthenticationOtp\Services\Auth\OTP as OTPService;
use Outgive\AuthenticationOtp\Exceptions\OTPException;

/**
 * This class describes an otp response.
 */
class OTPResponse
{
    /**
     * Gets the response.
     *
     * @param      string  $message     The message
     * @param      string  $otp_status  The otp status
     * @param      int     $code        The code
     *
     * @return     <type>  The response.
     */
    public function _getResponse(
        $message,
        $otp_status = "success",
        $code = 200
    )
    {
        return response([
            'otp_status' => $otp_status,
            'message'    => $message,
            'success'    => ($code == 200) ? true : false,
        ], $code);
    }

    /**
     * Gets the ready response.
     *
     * @param      string  $message     The message
     * @param      string  $otp_status  The otp status
     *
     * @return     resopnse  The ready response.
     */
    public function getReadyResponse(
        $message = null,
        $otp_status = OTPService::STATUS_READY
    )
    {
        if (!$message) {
            $message = "OTP Code refreshed";
        }

        return $this->_getResponse($message, $otp_status);
    }

    /**
     * Gets the verified response.
     *
     * @return     response  The verified response.
     */
    public function getVerifiedResponse()
    {
        return $this->_getResponse(
            'OTP code succesfully verified',
            OTPService::STATUS_VERIFIED
        );
    }

    /**
     * Gets the already verified response.
     *
     * @return     response  The already verified response.
     */
    public function getAlreadyVerified()
    {
        return $this->_getResponse(
            'OTP is already verified',
            OTPService::STATUS_ALREADY_VERFIED
        );
    }

    /**
     * Gets the error response.
     *
     * @param      Throwable  $exception  The exception
     *
     * @throws     Throwable  Original exception
     *
     * @return     response   The error response.
     */
    public function getErrorResponse(Throwable $exception)
    {
        $otp_status = "exception";

        if ($exception instanceof OTPException) {
            $otp_status = $exception->getStatus();
        } else {
            // throw $exception;
        }

        return $this->_getResponse(
            $exception->getMessage(),
            $otp_status,
            422
        );
    }

    /**
     * Gets the disabled response.
     */
    public function getDisabledResponse()
    {
        return $this->_getResponse(
            'OTP feature is disabled',
            OTPService::OTP_DISABLED,
            422
        );
    }

    /**
     * Gets the user not found.
     *
     * @return     response  The user not found.
     */
    public function getUserNotFound()
    {
        return $this->_getResponse(
            'User not found!',
            'error',
            422
        );
    }

    /**
     * Gets the disabled for user.
     *
     * @return     response  The disabled for user.
     */
    public function getDisabledForUser()
    {
        return $this->_getResponse(
            'OTP feature is disabled for this user',
            OTPService::OTP_DISABLED,
            422
        );
    }
}