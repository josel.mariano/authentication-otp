<?php

namespace Outgive\AuthenticationOtp\Tests\Feature\Auth;

/**
 * Vendors
 */
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Notification;
use outgive\AuthenticationLog\AuthenticationLog;
use Outgive\AuthenticationOtp\Notifications\SendOtp;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;
use Carbon\Carbon;
use Tests\TestCase;

/**
 * APP
 */
use App\Models\User;

/**
 * Package
 */
use Outgive\AuthenticationOtp\Models\Auth\Otp as AuthOtp;
use Outgive\AuthenticationOtp\Services\Auth\OTP as OTPService;
/**
 */
class AuthOtpTest extends TestCase
{
    use DatabaseTransactions;

    CONST USER_PHONE = "+639954340284";

    public function setUp(): void
    {
        parent::setUp();

        $this->app
            ->make(EloquentFactory::class)
            ->load(__DIR__.'/../../Factories');
    }

    public function test_assert()
    {
        $this->assertTrue(true);
    }

    /**
     * Test valid OTP
     */
    public function test_valid_otp()
    {
        $user = factory(User::class)->create([
            'phone'       => self::USER_PHONE,
            'otp_enabled' => 1,
        ]);

        $otp = factory(AuthOtp::class)->create([
            'user_id' => $user->id,
            'phone'   => $user->phone,
            'email'   => $user->email,
        ]);

        $response = $this->postJson('api/auth/otp/verify', [
            'email'    => $user->email,
            'password' => 'password',
            'code'     => $otp->code,
        ]);

        $otp = $otp->fresh();

        $response->assertStatus(200);
        $response->assertJson(['otp_status' => OTPService::STATUS_VERIFIED]);
        $response->assertJson(['success'    => true]);

        $response->assertHeaderMissing('Authorization');

        $this->assertEquals($otp->is_verified, 1);
    }

    /**
     * Test invalid OTP
     */
    public function test_invalid_otp()
    {
        $user = factory(User::class)->create([
            'phone'       => self::USER_PHONE,
            'otp_enabled' => 1,
        ]);

        $otp = factory(AuthOtp::class)->create([
            'user_id' => $user->id,
            'email'   => $user->email,
        ]);

        $response = $this->postJson('api/auth/otp/verify', [
            'email'    => $user->email,
            'password' => 'password',
            'code'     => 9999,
        ]);

        $otp = $otp->fresh();

        $response
            ->assertStatus(422)
            ->assertJson([
                'otp_status' => OTPService::STATUS_INVALID_OTP,
                'success'    => false,
            ]);
        $response->assertHeaderMissing('Authorization');

        $this->assertEquals($otp->is_verified, 0);
        $this->assertEquals($otp->code, 1234);
    }

    /**
     * Test already Verified OTP
     */
    public function test_already_verified_otp()
    {
        Notification::fake();

        $user = factory(User::class)->create([
            'phone'       => self::USER_PHONE,
            'otp_enabled' => 1,
        ]);

        $otp = factory(AuthOtp::class)->create([
            'user_id'     => $user->id,
            'phone'       => $user->phone,
            'email'       => $user->email,
            'code'        => 1234,
            'is_verified' => 1,
        ]);

        $authentication_log = factory(AuthenticationLog::class)->create([
            'authenticatable_id' => $user->id,
        ]);

        $response = $this->postJson('api/auth/login', [
            'email'    => $user->email,
            'password' => 'password',
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => 'success',
            ])
            ->assertHeader('Authorization');

        $response = $this->postJson('api/auth/otp/check', [
            'email'    => $user->email,
            'password' => 'password',
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'otp_status' => OTPService::STATUS_ALREADY_VERFIED,
                'success'    => true,
            ]);

        // Assert a notification was sent to the given users...
        Notification::assertNotSentTo(
            [$user],
            SendOtp::class
        );
    }

    /**
     * Test login OTP
     */
    public function test_login_otp()
    {
        Notification::fake();

        $user = factory(User::class)->create([
            'phone'       => self::USER_PHONE,
            'otp_enabled' => 1,
        ]);

        $otp = factory(AuthOtp::class)->create([
            'user_id'     => $user->id,
            'phone'       => $user->phone,
            'email'       => $user->email,
            'code'        => 1234,
            'is_verified' => 0,
        ]);

        $response = $this->postJson('api/auth/login', [
            'email'    => $user->email,
            'password' => 'password',
        ]);

        $auth_otp = AuthOtp::where('user_id', $user->id)->first();

        $response
            ->assertStatus(200)
            ->assertJson([
                'otp_status' => OTPService::STATUS_READY,
                'success'    => true,
            ]);
        $response->assertHeaderMissing('Authorization');

        $this->assertEquals($auth_otp->user_id, $user->id);
        $this->assertEquals($auth_otp->email, $user->email);
        $this->assertEquals($auth_otp->is_verified, 0);


        // Assert a notification was sent to the given users...
        Notification::assertSentTo(
            [$user],
            SendOtp::class
        );
    }

    /**
     * Test Expired OTP
     */
    public function test_expired_otp()
    {
        Notification::fake();

        $expire_seconds = 1 + (int)config('otp.expire_seconds');
        $now = Carbon::now();
        $end = Carbon::parse($now)->addSeconds($expire_seconds);
        $sec = $end->diffInSeconds($now);

        $user = factory(User::class)->create([
            'phone'       => self::USER_PHONE,
            'otp_enabled' => 1,
        ]);

        $otp = factory(AuthOtp::class)->create([
            'user_id'    => $user->id,
            'phone'      => $user->phone,
            'email'      => $user->email,
            'code'       => 1234,
            'created_at' => date('1999-m-d H:i:s'),
            'updated_at' => date('1999-m-d H:i:s'),
        ]);

        $authentication_log = factory(AuthenticationLog::class)->create([
            'authenticatable_id' => $user->id,
        ]);

        $response = $this->postJson('api/auth/otp/verify', [
            'email'    => $user->email,
            'password' => 'password',
            'code' => 1234,
        ]);

        $response
            ->assertStatus(422)
            ->assertJson([
                'otp_status' => OTPService::STATUS_EXPIRED_OTP,
                'success'    => false,
            ]);
        $response->assertHeaderMissing('Authorization');

        // Assert a notification was sent to the given users...
        Notification::assertNotSentTo(
            [$user],
            SendOtp::class
        );
    }

    /**
     * Test verified_Expired OTP
     */
    public function test_verified_expired_otp()
    {
        Notification::fake();

        $user = factory(User::class)->create([
            'phone'       => self::USER_PHONE,
            'otp_enabled' => 1,
        ]);

        $plus_days = 2 + (int)config('otp.force_expire_days');
        $otp = factory(AuthOtp::class)->create([
            'user_id'     => $user->id,
            'phone'       => $user->phone,
            'email'       => $user->email,
            'code'        => 1234,
            'is_verified' => 1,
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()->addDays($plus_days),
        ]);

        $authentication_log = factory(AuthenticationLog::class)->create([
            'authenticatable_id' => $user->id,
        ]);

        $response = $this->postJson('api/auth/otp/check', [
            'email'    => $user->email,
            'password' => 'password',
        ]);

        $otp = $otp->fresh();

        $response
            ->assertStatus(200)
            ->assertJson([
                'otp_status' => OTPService::STATUS_READY,
                'success'    => true,
            ]);
        $response->assertHeaderMissing('Authorization');

        $this->assertEquals($otp->is_verified, 0);
        $this->assertTrue($otp->code != 1234);


        // Assert a notification was sent to the given users...
        Notification::assertSentTo(
            [$user],
            SendOtp::class
        );
    }

    /**
     * Test resumable otp, unverified but not expired
     */
    public function test_resume_existing_otp()
    {
        Notification::fake();

        $user = factory(User::class)->create([
            'phone'       => self::USER_PHONE,
            'otp_enabled' => 1,
        ]);

        $otp = factory(AuthOtp::class)->create([
            'user_id'     => $user->id,
            'phone'       => $user->phone,
            'email'       => $user->email,
            'code'        => 1234,
            'is_verified' => 0,
        ]);

        $authentication_log = factory(AuthenticationLog::class)->create([
            'authenticatable_id' => $user->id,
        ]);

        $response = $this->postJson('api/auth/otp/check', [
            'email'    => $user->email,
            'password' => 'password',
        ]);

        $otp = $otp->fresh();

        $response
            ->assertStatus(200)
            ->assertJson([
                'otp_status' => OTPService::STATUS_READY,
                'success'     => true,
            ]);
        $response->assertHeaderMissing('Authorization');

        $this->assertEquals($otp->user_id, $user->id);
        $this->assertEquals($otp->email, $user->email);
        $this->assertEquals($otp->is_verified, 0);
        $this->assertEquals($otp->code, 1234);

        $response = $this->postJson('api/auth/otp/verify', [
            'email'    => $user->email,
            'password' => 'password',
            'code'     => $otp->code,
        ]);

        $otp = $otp->fresh();

        $response
            ->assertStatus(200)
            ->assertJson([
                'otp_status' => OTPService::STATUS_VERIFIED,
                'success'    => true,
            ]);
        $response->assertHeaderMissing('Authorization');
        $this->assertEquals($otp->is_verified, 1);

        // Assert a notification was sent to the given users...
        Notification::assertNotSentTo(
            [$user],
            SendOtp::class
        );
    }

    /**
     * Test OTP Unfamiliar ip
     */
    public function test_otp_unfamiliar_ip()
    {
        Notification::fake();

        $user = factory(User::class)->create([
            'phone'       => self::USER_PHONE,
            'otp_enabled' => 1,
        ]);

        $otp = factory(AuthOtp::class)->create([
            'user_id'     => $user->id,
            'phone'       => $user->phone,
            'email'       => $user->email,
            'code'        => 1234,
            'is_verified' => 1,
        ]);

        $authentication_log = factory(AuthenticationLog::class)->create([
            'ip_address'         => '9.9.9.9',
            'authenticatable_id' => $user->id,
        ]);

        $response = $this
            ->postJson(
                'api/auth/login',
                [
                    'email'    => $user->email,
                    'password' => 'password',
                ]
            );

        $otp = $otp->fresh();

        $response
            ->assertStatus(200)
            ->assertJson([
                'otp_status' => OTPService::STATUS_READY,
                'success'     => true,
            ]);
        $response->assertHeaderMissing('Authorization');

        $this->assertEquals($otp->user_id, $user->id);
        $this->assertEquals($otp->email, $user->email);
        $this->assertEquals($otp->is_verified, 0);

        // Assert a notification was sent to the given users...
        Notification::assertSentTo(
            [$user],
            SendOtp::class
        );
    }

    /**
     * Test User enable otp
     */
    public function test_enable_otp()
    {
        Notification::fake();

        $user = factory(User::class)->create([
            'phone'       => self::USER_PHONE,
            'otp_enabled' => 0,
        ]);

        $this->localActingAs($user);
        $post_data = [
            'email'   => $user->email,
            'otp_via' => OTPService::VIA_SMS,
        ];

        $response = $this->postJson(
            'api/auth/otp/enable',
            $post_data
        );

        $response->assertStatus(422);
        $response->assertJson(['errors'  =>
            ['phone' => ['The phone field is required.']],
        ]);

        $post_data['otp_via'] = OTPService::VIA_SMS;
        $post_data['phone']   = self::USER_PHONE;

        $response = $this->postJson(
            'api/auth/otp/enable',
            $post_data
        );

        $otp  = AuthOtp::where('user_id', $user->id)->first();
        $user = $user->fresh();

        $this->assertEquals($user->phone, self::USER_PHONE);
        $this->assertEquals($otp->phone, self::USER_PHONE);
        $this->assertEquals($otp->otp_enabled, 0);
        $this->assertEquals($otp->otp_via, OTPService::VIA_SMS);

        // Assert a notification was sent to the given users...
        Notification::assertSentTo(
            [$user],
            SendOtp::class
        );
    }

    /**
     * Test User disable otp
     */
    public function test_disable_otp()
    {
        $user = factory(User::class)->create([
            'phone'       => self::USER_PHONE,
            'otp_enabled' => 1,
        ]);

        $this->localActingAs($user);
        $post_data = [
            'email' => $user->email,
        ];

        $response = $this->postJson('api/auth/otp/disable');

        $response
            ->assertStatus(200)
            ->assertJson([
                'success'     => true,
            ]);

        $user = $user->fresh();
        $otp  = AuthOtp::where('user_id', $user->id)->first();

        $this->assertEquals($user->otp_enabled, 0);
        $this->assertEquals($otp->otp_enabled, 0);
    }

    public function test_otp_activate()
    {
        $user = factory(User::class)->create([
            'phone'       => self::USER_PHONE,
            'otp_enabled' => 1,
        ]);


        $otp = factory(AuthOtp::class)->create([
            'user_id'     => $user->id,
            'phone'       => $user->phone,
            'email'       => $user->email,
            'code'        => 1234,
            'is_verified' => 1,
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now(),
        ]);

        $authentication_log = factory(AuthenticationLog::class)->create([
            'authenticatable_id' => $user->id,
        ]);

        $this->localActingAs($user);
        $response = $this->postJson('api/auth/otp/activate', [
            'code' => '1234',
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'otp_status' => OTPService::STATUS_READY,
                'success'    => true,
                'success'    => 'OTP Activated',
            ]);
        $response->assertHeaderMissing('Authorization');

        $otp = $otp->fresh();
        $this->assertEquals($otp->is_verified, 1);
        $this->assertEquals($otp->otp_enabled, 1);
        $this->assertTrue($otp->code == 1234);
    }

    /**
     * Test Refresh otp code
     */
    public function test_refresh_otp_code()
    {
        Notification::fake();

        $user = factory(User::class)->create([
            'phone'       => self::USER_PHONE,
            'otp_enabled' => 1,
        ]);

        $otp = factory(AuthOtp::class)->create([
            'user_id'     => $user->id,
            'phone'       => $user->phone,
            'email'       => $user->email,
            'code'        => 1234,
            'is_verified' => 1,
        ]);

        $queries = [
            'email' => $user->email,
        ];

        $response = $this->getJson(
            'api/auth/otp/refresh' . "?" . http_build_query($queries)
        );

        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $otp = $otp->fresh();

        $this->assertTrue($otp->code != 1234);

        // Assert a notification was sent to the given users...
        Notification::assertSentTo(
            [$user],
            SendOtp::class
        );
    }

    /**
     * Test Refresh otp code vie email
     */
    public function test_email_otp_code()
    {
        Notification::fake();

        $user = factory(User::class)->create([
            'phone'       => self::USER_PHONE,
            'otp_enabled' => 1,
        ]);

        $otp = factory(AuthOtp::class)->create([
            'user_id'     => $user->id,
            'phone'       => $user->phone,
            'email'       => $user->email,
            'code'        => 1234,
            'is_verified' => 1,
        ]);

        $queries = [
            'email' => $user->email,
        ];

        $response = $this->getJson(
            'api/auth/otp/email' . "?" . http_build_query($queries)
        );

        $response
            ->assertStatus(200)
            ->assertJson([
                'success'     => true,
            ]);

        $otp = $otp->fresh();
        $this->assertTrue($otp->code != 1234);

        // Assert a notification was sent to the given users...
        Notification::assertSentTo(
            [$user],
            SendOtp::class
        );

    }

}
