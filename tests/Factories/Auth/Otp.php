<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Outgive\AuthenticationOtp\Services\Auth\OTP as OTPService;
use Outgive\AuthenticationOtp\Models\Auth\Otp as AuthOtp;
use Carbon\Carbon;

$factory->define(AuthOtp::class, function () {
    return [
        'created_at'  => date('Y-m-d H:i:s'),
        'updated_at'  => date('Y-m-d H:i:s'),
        'phone'       => '+6399999999',
        'otp_enabled' => 1,
        'otp_via'     => OTPService::VIA_SMS,
        'code'        => 1234,
    ];
});
